CHANGELOG
=========

This changelog references the relevant changes (bug and security fixes)

* 0.2.0 (2019-06-25)

    * change [CrudService] Changed Crud Service methods params to make params and config easier
    * change [ApiService] Changed Api Service methods params to make params and config easier

* 0.1.0 (2019-06-24)

    * feature [CrudService] Added Crud Service axios config param
    * change [ApiService] Changed Api Service methods to set axios config param
